package main

import (
	"fmt"
	"os"
	"bitbucket.org/vellamike/sainsburys/sainscraper"
)

func main() {
	var url string

	if len(os.Args) > 1 {
		url = os.Args[1]
	} else {
		url = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html"
	}

	productURLs := sainscraper.ProductURLsFromProductPage(url)
	var productSummary sainscraper.ProductSummary
	productSummary.AddProductFromURLs(productURLs)
	fmt.Println(productSummary.AsJson())
}
