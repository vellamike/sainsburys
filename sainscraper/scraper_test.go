package sainscraper

import "testing"

func TestSanitizePriceWithGoodValues(t *testing.T) {
	price_formats := []string{"£1.40", "$1.40", "1.40", "Price is £1.40 per unit"}
	var s string
	for _, p := range price_formats {
		s, _ = sanitizePrice(p)
		if s != "1.40" {
			t.Error("Expected 1.40 for input of ", p, "instead got ", s)
		}
	}
}

func TestSanitizePriceWithIncorrectValues(t *testing.T) {
	price_formats := []string{"No price here", "Malformed price: $1. 30", "140", "140."}

	for _, p := range price_formats {
		s, err := sanitizePrice(p)
		if err == nil {
			t.Error("Expected error for input of ", p, "but instead matched:", s)
		}
	}
}

func TestJSONMarshallerUnescapedString(t *testing.T) {
	input_string := []byte("Hello, World!")
	json_unescaped, _ := jsonMarshal(input_string, false)
	json_escaped, _ := jsonMarshal(input_string, true)
	if len(json_escaped) != len(json_unescaped) {
		t.Error("String changed")
	}

	for i, _ := range json_unescaped {
		if json_escaped[i] != json_unescaped[i] {
			t.Error("Strings changed")
		}
	}
}
