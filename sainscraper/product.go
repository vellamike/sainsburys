package sainscraper

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/shopspring/decimal"
	"regexp"
)

func sanitizePrice(unsanitizedPrice string) (string, error) {
	// Remove currency and other information from price, returning only price as a number
	// The string "£1.43 / kg" for instance will be returned as "1.43"
	re := regexp.MustCompile("[0-9]+\\.[0-9]+")
	productPriceMatches := re.FindAllString(unsanitizedPrice, -1)
	if len(productPriceMatches) == 0 {
		return "", errors.New("No price found in string")
	}
	productPrice := productPriceMatches[0]
	return productPrice, nil
}

type product struct {
	Price       string
	Title       string
	Description string
	Size        string
}

func (self product) priceAsDecimal() (decimal.Decimal, error) {
	productPrice, err := sanitizePrice(self.Price)
	if err == nil {
		price, _ := decimal.NewFromString(productPrice)
		return price, nil
	} else {
		return decimal.Zero, errors.New("Could not get a price")
	}
}

type ProductSummary struct {
	Products []product
	Total    decimal.Decimal
}

func (self *ProductSummary) AddProduct(product product) {
	self.Products = append(self.Products, product)
	price, _ := product.priceAsDecimal()
	self.Total = self.Total.Add(price)
}

func (self *ProductSummary) AddProductFromURLs(urls []string) {
	resc := make(chan product)

	for _, url := range urls {
		go func(url string) {
			newProduct := productFromURL(url)
			resc <- newProduct
		}(url)
	}
	for i := 0; i < len(urls); i++ {
		self.AddProduct(<-resc)
	}
}

func jsonMarshal(v interface{}, safeEncoding bool) ([]byte, error) {
	// the json marshaller in golang is urlsafe, so escaping needs to be fixed
	b, err := json.Marshal(v)

	if safeEncoding {
		b = bytes.Replace(b, []byte("\\u003c"), []byte("<"), -1)
		b = bytes.Replace(b, []byte("\\u003e"), []byte(">"), -1)
		b = bytes.Replace(b, []byte("\\u0026"), []byte("&"), -1)
	}
	return b, err
}

func (self ProductSummary) AsJson() string {
	summaryAsJson, _ := jsonMarshal(self, true)
	return string(summaryAsJson)
}
