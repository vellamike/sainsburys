package sainscraper

import (
	"github.com/yhat/scrape"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"net/http"
	"strconv"
)

func productFromURL(productURL string) product {
	// Parse a product page and return a product object
	resp, err := http.Get(productURL)
	if err != nil {
		panic(err)
	}
	root, err := html.Parse(resp.Body)
	if err != nil {
		panic(err)
	}

	// get the response size
	responseSizeKilobytes := float64(resp.ContentLength) / 1000.0
	size := strconv.FormatFloat(responseSizeKilobytes, 'f', 2, 64) + "kB"

	// define a matcher for the product title
	titleMatcher := func(n *html.Node) bool {
		// must check for nil values
		if n.DataAtom == atom.H1 && n.Parent != nil {
			return scrape.Attr(n.Parent, "class") == "productTitleDescriptionContainer"
		}
		return false
	}

	// define a matcher for the product price
	priceMatcher := func(n *html.Node) bool {
		// must check for nil values
		if n.DataAtom == atom.P {
			return scrape.Attr(n, "class") == "pricePerUnit"
		}
		return false
	}

	// define a matcher for the product description
	descriptionMatcher := func(n *html.Node) bool {
		// must check for nil values
		if n.DataAtom == atom.P && n.Parent != nil {
			return scrape.Attr(n.Parent, "class") == "productText"
		}
		return false
	}

	// get all product titles
	titles := scrape.FindAll(root, titleMatcher)
	productTitle := scrape.Text(titles[0])

	// get all product prices
	prices := scrape.FindAll(root, priceMatcher)
	productPriceUnsanitized := scrape.Text(prices[0])
	price, _ := sanitizePrice(productPriceUnsanitized)

	// get all product descriptions
	descriptions := scrape.FindAll(root, descriptionMatcher)
	productDescription := scrape.Text(descriptions[0])
	productDetail := product{price, productTitle, productDescription, size}

	return productDetail
}

func ProductURLsFromProductPage(productsURL string) []string {
	// request and parse the front page, returning a list with one URL per product
	resp, err := http.Get(productsURL)
	if err != nil {
		panic(err)
	}
	root, err := html.Parse(resp.Body)
	if err != nil {
		panic(err)
	}

	// define a matcher
	matcher := func(n *html.Node) bool {
		// must check for nil values
		if n.DataAtom == atom.A && n.Parent != nil && n.Parent.Parent != nil {
			return scrape.Attr(n.Parent.Parent, "class") == "productInfo"
		}
		return false
	}

	productElements := scrape.FindAll(root, matcher)
	var productURLs []string
	for _, p := range productElements {
		productURLs = append(productURLs, scrape.Attr(p, "href"))
	}
	return productURLs
}
