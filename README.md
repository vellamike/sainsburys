# Sainsburys web scraper

Scraping a sainsburys website to collect some product summary information. Makes use of the [yhat go web scraper](https://github.com/yhat/scrape). Also makes use of the [shopspring decimal](https://github.com/shopspring/decimal) package to work with prices.

To make the test more interesting, the products are fetched concurrently using goroutines and channels.

## Installation and running

```
$ go get bitbucket.org/vellamike/sainsburys
$ $GOPATH/bin/sainsburys
```

You can also specify a URL:

```
$ $GOPATH/bin/sainsburys http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html
```

# Running tests

```
$ cd $GOPATH/src/bitbucket.org/vellamike/sainsburys/sainscraper
$ go test
```
